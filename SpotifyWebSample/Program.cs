﻿using SpotifyWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWebSample
{
    class Program
    {
        static void Main(string[] args)
        {
            SpotifyApi api = new SpotifyApi();

            var art = api.SearchArtists("Bob");
            api.Lookup(art[2], LookupDetail.Full);
            var alb = api.SearchAlbums(art[0].Name);
            api.Lookup(alb[0], LookupDetail.Full);
            var tra = api.SearchTracks("Starry Eyed");
            Console.WriteLine(api.Lookup(tra[0]).Available);
        }
    }
}
