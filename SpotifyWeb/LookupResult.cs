﻿namespace SpotifyWeb
{
    public class LookupResult
    {
        public LookupInfo Info { get; set; }
    }

    public class LookupInfo
    {
        public SpotifyResourceType Type { get; set; }
    }

    public enum LookupDetail
    {
        None,
        Basic,
        Full,
    }
}
