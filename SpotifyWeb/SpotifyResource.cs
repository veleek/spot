﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class SpotifyResource
    {
        public SpotifyResource(SpotifyResourceType type)
        {
            this.Type = type;
        }

        public string HRef { get; set; }
        public string Name { get; set; }
        public float? Popularity { get; set; }

        public SpotifyResourceType Type { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1:0.#####})", Name, Popularity ?? 0);
        }
    }
}
