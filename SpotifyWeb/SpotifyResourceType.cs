﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public enum SpotifyResourceType
    {
        Album,
        Artist,
        Track,
    }
}
