﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class ExternalId
    {
        public string Type { get; set; }
        public string Id { get; set; }
    }
}
