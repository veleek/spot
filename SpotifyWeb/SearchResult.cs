﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class SearchResult
    {
        public SearchInfo Info { get; set; }
        public List<Artist> Artists { get; set; }
        public List<Album> Albums { get; set; }
        public List<Track> Tracks { get; set; }
    }

    public class SearchInfo
    {
        public int NumResults { get; set; }
        public int Limit { get; set; }
        public int Offest { get; set; }
        public string Query { get; set; }
        public SpotifyResourceType Type { get; set; }
        public int Page { get; set; }
    }
}
