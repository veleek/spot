﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class Artist : SpotifyResource
    {
        public Artist() : base(SpotifyResourceType.Artist) { }

        public List<Album> AlbumList
        {
            get
            {
                return Albums.Select(alr => alr.Album).ToList();
            }
        }
        public List<AlbumLookupResult> Albums { get; set; }
    }

    public class ArtistLookupResult : LookupResult
    {
        public Artist Artist { get; set; }
    }
}
