﻿using RestSharp;
using System;
using System.Collections.Generic;

namespace SpotifyWeb
{
    public class SpotifyApi
    {
        public const string BaseUrl = "http://ws.spotify.com";
        public const string ResourcePathFormat = "/{0}/{1}/{2}";

        private RestClient client;

        public int Version { get; set; }

        public SpotifyApi() : this(1)
        {
        }

        public SpotifyApi(int version)
        {
            this.Version = version;

            this.client = new RestClient(BaseUrl);
        }

        public SearchResult Search(SpotifyResourceType resourceType, string query, int? page = null, SpotifyResponseFormat? responseFormat = null)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentNullException("query");
            }

            if (page.HasValue && page.Value < 1)
            {
                throw new ArgumentOutOfRangeException("page", "page must be greater than or equal to 1");
            }

            RestRequest req = new RestRequest(GetResourcePath("search", resourceType.ToString(), SpotifyResponseFormat.Json));
            
            req.AddParameter("q", query);
            if (page.HasValue)
            {
                req.AddParameter("page", page.Value);
            }

            var resp = client.Execute<SearchResult>(req);

            if (resp.ResponseStatus != ResponseStatus.Completed)
            {
                if (resp.ErrorException != null)
                {
                    throw resp.ErrorException;
                }
                else
                {
                    throw new Exception(resp.ErrorMessage);
                }
            }

            return resp.Data;
        }

        public List<Artist> SearchArtists(string query, int? page = null)
        {
            var result = Search(SpotifyResourceType.Artist, query, page);
            return result.Artists;
        }

        public List<Album> SearchAlbums(string query, int? page = null)
        {
            return Search(SpotifyResourceType.Album, query, page).Albums;
        }

        public List<Track> SearchTracks(string query, int? page = null)
        {
            return Search(SpotifyResourceType.Track, query, page).Tracks;
        }

        public Artist Lookup(Artist artist, LookupDetail detail = LookupDetail.None)
        {
            return Lookup<ArtistLookupResult>(artist, detail).Artist;
        }

        public Album Lookup(Album album, LookupDetail detail = LookupDetail.None)
        {
            return Lookup<AlbumLookupResult>(album, detail).Album;
        }

        public Track Lookup(Track track)
        {
            return Lookup<TrackLookupResult>(track).Track;
        }

        public TLookupResult Lookup<TLookupResult>(SpotifyResource resource, LookupDetail detail = LookupDetail.None)
            where TLookupResult : LookupResult, new()
        {
            return Lookup<TLookupResult>(resource.HRef, detail);
        }

        public TLookupResult Lookup<TLookupResult>(string resourceUri, LookupDetail detail = LookupDetail.None)
            where TLookupResult : LookupResult, new()
        {
            RestRequest req = new RestRequest(GetResourcePath("lookup", String.Empty, SpotifyResponseFormat.Json));

            req.AddParameter("uri", resourceUri);

            if (detail != LookupDetail.None)
            {
                if (resourceUri.Contains("artist"))
                {
                    req.AddParameter("extras", "album" + (detail == LookupDetail.Full ? "detail": string.Empty));
                }
                else if (resourceUri.Contains("album"))
                {
                    req.AddParameter("extras", "track" + (detail == LookupDetail.Full ? "detail" : string.Empty));
                }
            }

            var resp = client.Execute<TLookupResult>(req);

            if (resp.ResponseStatus != ResponseStatus.Completed)
            {
                if (resp.ErrorException != null)
                {
                    throw resp.ErrorException;
                }
                else
                {
                    throw new Exception(resp.ErrorMessage);
                }
            }

            return resp.Data;
        }

        private string GetResourcePath(string service, string method, SpotifyResponseFormat? responseFormat = null)
        {
            string resourcePath = string.Format(ResourcePathFormat, service, Version, method);

            if (responseFormat != null)
            {
                resourcePath += "." + responseFormat.Value;
            }

            return resourcePath;
        }
    }

    public enum SpotifyResponseFormat
    {
        Xml,
        Json,
    }
}
