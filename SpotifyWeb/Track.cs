﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class Track : SpotifyResource
    {
        public Track() : base(SpotifyResourceType.Track) { }

        public Album Album { get; set; } 
        public List<Artist> Artists { get; set; }
        public int TrackNumber { get; set; }
        public int DiscNumber { get; set; }
        public float Length { get; set; }
        public Boolean Available { get; set; }

        public List<ExternalId> ExternalIds { get; set; }
    }

    public class TrackLookupResult : LookupResult
    {
        public Track Track { get; set; }
    }
}
