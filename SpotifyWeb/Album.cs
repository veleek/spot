﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyWeb
{
    public class Album : SpotifyResource
    {
        private List<Track> tracks;

        public Album() : base(SpotifyResourceType.Album) { }

        public string Artist
        {
            get
            {
                return (Artists == null || Artists.Count == 0) ? null : Artists[0].Name;
            }
            set
            {
                if (Artists == null)
                {
                    Artists = new List<Artist> { new Artist() };
                }
                Artists[0].Name = value;
            }
        }
        public string ArtistId
        {
            get
            {
                return (Artists == null || Artists.Count == 0) ? null : Artists[0].HRef;
            }
            set
            {
                if (Artists == null)
                {
                    Artists = new List<Artist> { new Artist() };
                }
                Artists[0].HRef = value;
            }
        }

        /// <summary>
        /// Gets or sets the year that an album was released
        /// </summary>
        public int Released { get; set; }

        public List<Track> Tracks
        {
            get { return tracks; }
            set
            {
                tracks = value;
                foreach (var track in tracks)
                {
                    track.Album = this;
                }
            }
        }

        public List<Artist> Artists { get; set; }
        public Availability Availability { get; set; }
        public List<ExternalId> ExternalIds { get; set; }
    }

    public class AlbumLookupResult : LookupResult
    {
        public Album Album { get; set; }
    }

    public class Availability
    {
        public string Territories { get; set; }
    }
}
